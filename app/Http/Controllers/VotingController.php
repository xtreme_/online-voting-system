<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\VoterDetail;
use App\CandidateDetail;
//use App\ConstituencyBlockDetail;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;


class VotingController extends Controller
{

   

   public function create()
   {
      return view('voting.create');
   }
   public function store(Request $request)
   {
      $voters =$request->get('voterid');
      $user = DB::table('voter_details')->where('voterid', $voters)->first();
     $aa=$user->block;
      //return $aa;
     $ash = DB::table('constituency_block_details')->where('block',$aa)->first();
     $bb=$ash->constituencyid;
      $kmr=DB::table('candidate_details')->where('constituencyid',$bb)->get();
      //return $kmr->candidatename;
      return view('voting.show',compact('kmr'));
   }

}
