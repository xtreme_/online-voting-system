<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testElectionNav()
    {
        $this->visit('/')
            ->click('Election')
            ->seePageIs('/elections/create');
    }

    public function testElectionForm()
    {
        $this->visit('/elections/create')
            ->type('2016', 'year')
            ->type('state', 'status')
            ->type('bihar', 'state')
            ->type('2016-05-09', 'start')
            ->type('2016-06-09', 'end')
            ->press('Create Election!')
            ->seePageIs('/elections');
    }

    public function testDatabase()
    {
        // Make call to application...

        $this->seeInDatabase('elections', ['electionid' => '1']);
    }
}
