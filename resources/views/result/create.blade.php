

@extends('layouts.master')

@section('title', 'Party Registration')

@section('sidebar')
    @parent
    <h3><u>Add Party</u></h3>
@endsection

@section('content')



    {!! Form::open(
      array(
        'route' => 'result.store',
        'class' => 'form')
      ) !!}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            There were some problems adding the category.<br />
            <ul>
                @foreach ($errors->all() as $error)
                    <li></li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('ElectionID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') !!}
        {!! Form::text('electionid', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'name'
          )) !!} </div><br>
    <div class="form-group">
        {!! Form::label('ConstituencyID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') !!}
        {!! Form::text('constituencyid', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'name'
          )) !!}</div><br>

    <div class="form-group">
        {!! Form::submit('ADD',
          array('class'=>'btn btn-primary'
        )) !!}
    </div>
    {!! Form::close() !!}
    </div>
@endsection
