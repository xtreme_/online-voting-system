@extends('layouts.master')
@section('title', 'Create Election')
@section('sidebar')
    @parent

@endsection

@section('content')
    <h1 font="bold">Create Election</h1>

    {!! Form::open(
      array(
        'route' => 'elections.store',
        'class' => 'form')
      ) !!}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            There were some problems adding the category.<br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li></li>
                @endforeach
            </ul>
        </div>
    @endif
<table>
  <tr>  <div class="form-group">
      <td> {!! Form::label('Year') !!}</td>
       <td> {!! Form::text('year', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'Year'
          )) !!}</td> </div></tr>
    <tr>  <div class="form-group">
         <td>   {!! Form::label('Type') !!}</td>
            <td>   {!! Form::text('status', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'type'
          )) !!}</td>  </div></tr>
    <div class="form-group">
        <tr> <td>  {!! Form::label('State') !!} </td>
            <td>  {!! Form::text('state', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'Name'
          )) !!} </td></tr> </div>
    <div class="form-group">
        <tr> <td>   {!! Form::label('Start') !!}</td>
            <td> {!! Form::text('start', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'YYYY-MM-DD'
          )) !!}</td></tr></div>
    <div class="form-group">
        <tr> <td>  {!! Form::label('End') !!}</td>
            <td>   {!! Form::text('end', null,
          array(
            'class'=>'form-control',
            'placeholder'=>'YYYY-MM-DD'
          )) !!}</td></tr> </div>

    </div></table>
    <div class="form-group">
    <div class="form-group">
        {!! Form::submit('Create Election!',
          array('class'=>'btn btn-primary'
        )) !!}</div>
    </div>
    {!! Form::close() !!}
    </div>
@endsection


</tr>